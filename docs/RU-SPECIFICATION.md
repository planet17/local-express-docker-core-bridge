
Окружение:

    UI: PHP 7.x, Yii 2.0.x,
    DB: MySQL 5.7+



В базе есть множество таблиц, но для выполнения задания необходимы следующие:

1. store

        id, title

2. store_product

        id, store_id, upc, title, price


Задание:

В систему необходимо добавить новую функциональность — импорт данных из разных файлов.
В рамках задания необходимо реализовать только импорт из CSV файлов, но спроектировать систему необходимо таким образом,
что бы добавление поддержки новых типов файлов (к примеру xlsx) не вызывало бы проблем.

Формат CSV файла:

    1. В первой строке находятся идентификаторы столбцов
    2. Порядок столбцов может меняться. Список поддерживаемых столбцов: upc,title,price.
       upc - обязателен. В противном случае все товары импорта должны быть помечены как ошибочные.
    3. Далее идут данные продуктов

Пример CSV файла:

    title,upc,price
    Broccoli,0101010101,2.99

Или:

    upc,price
    0101010101,2.99

В UI пользователь должен иметь возможность открыть 2 страницы:

    1. Страница с выбором файла с данными (до 5 Mb) и магазина для которого должен будет происходить импорт
    2. Страница, где пользователь должен иметь возможность просматривать список всех ипортов c их состояниями,
       номером импорта, названием магазина, а также кол-вом импортированных продуктов и ошибочных на данный момент.

После загрузки файла пользовать должен автоматически попадать на список всех импортов.
Реализовывать автоматическое обновление статусов импортов, в рамках данной задачи, не требуется.

Сервис процессинга файлов импорта:

    1. Обработка файлов импорта должна происходить ассинхронно. Пользователь не должен ждать пока
       данные будут обработаны.
    2. Сервис должен обеспечивать обработку 2х файлов одновременно в один момент времени
       (константа, которую можно менять)
    3. Если были загружены несколько файлов импорта для одного магазина,
       то они должны обрабатываться последовательно, согласно времени и дате импорта.
    4. Импорт может иметь следующие состояния: NEW, PROCESSING, DONE
    5. Поиск товара должен происходить по UPC. Если товар найден в данном магазине,
       то необходимо обновить title и price (если они присутствуют в файле импорта). Если товар не найден,
       то необходимо его создать, если есть все необходимые данные (upc,title,price).
    6. Для реализации это сервиса, а так же его связи с UI,
       Вы можете использовать любые языки, фреймворки и механизмы, а так же добавлять все необходимые таблицы в DB.

