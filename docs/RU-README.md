# Local Express: Bridge (RU) #

https://bitbucket.org/planet17/local-express-docker-core-bridge

Контейнер содержащий образ очереди сообщений и отдельная база данных для целостности/стойкости сообщений.


-------------------


Весь проект разработан с использованием [Docker](https://www.docker.com/).

`Bridge` - это докер образ и в тот же момент центральная часть проекта.

Эту же документацию на английском можно найти в корне проекта [/README.md](/README.md)

Этот проект (часть) содержит в себе образ очереди сообщений (Gearman), базу данных для очереди, документацию,
описание, инструкции. Также, он содержит директорию для хранения загружаемых файлов. (быстрый способ для 
имитации CDN). В этом проекте всегда содержится самый полный файл как образец переменных настройки окружения - `.env.
example`.

Здесь можно прочитать саму задачу тестового задания `/docs/RU-SPECIFICATION.md`.


## Окружение ##


-------------------


    Docker: *
    Mysql: 5.7.30
    Gearmand 1.0.6


-------------------


## Части проекта ##


Тестовое задание выполнено с такими отдельными docker контейнерами:

*Обязательные:*

1) [Bridge (current)](https://bitbucket.org/planet17/local-express-docker-core-bridge) - содержит образ очереди 
   сообщений, документацию.

2) [Database](https://bitbucket.org/planet17/local-express-docker-storage-mysql) - содержит общую центральную базу 
   данных.

3) [Yii2 - Client Application](https://bitbucket.org/planet17/local-express-docker-client-app) - клиентское приложение.

4) [Processing](https://bitbucket.org/planet17/local-express-docker-processing-app) - сервисное приложение для 
   тяжелых задач.

*Дополнительные:*

5) [Dashboard for Gearman](https://bitbucket.org/planet17/local-express-docker-gearman-dashboard) -
   можно использовать для просмотра состояния демонов, очередей, и сообщений.

### Не отдельный проект (не требует отдельной установки), но также часть работы ###

#### Библиотека домена (области/сферы) ####

[CQO](https://bitbucket.org/planet17/local-express-common-queue-objects) - библиотека которая
в обоих приложениях (`Processing` и `Yii2`). Содержит в себе общие используемые элементы для работы с очередями 
сообщений.

#### Библиотеки не домена (для области/сферы): ####

* [https://bitbucket.org/planet17/message-queue-library](https://bitbucket.org/planet17/message-queue-library)
* [https://bitbucket.org/planet17/message-queue-library-route-nav](https://bitbucket.org/planet17/message-queue-library-route-nav)
* [https://bitbucket.org/planet17/message-queue-library-process-manager](https://bitbucket.org/planet17/message-queue-library-process-manager)
* [https://bitbucket.org/planet17/cli-processes](https://bitbucket.org/planet17/cli-processes)
* [https://bitbucket.org/planet17/application-process-manager-routed-queue](https://bitbucket.org/planet17/application-process-manager-routed-queue)
* [https://bitbucket.org/planet17/message-queue-library-pipeline-handlers](https://bitbucket.org/planet17/message-queue-library-pipeline-handlers)


-------------------

##### Почему я написал эти библиотеки? #####

* Еще давно... Я запланировал написать удобные обертки для работы с разными очередями сообщений.
  Несколько раз я пытался сделать это. Но как результат у меня было несколько заготовок, и нерешенные проблемы с кодом.
  Это тестовое задание мотивировало меня не использовать готовый где-то функционал, а взять, удалить все наработки и 
  с чистого листа закончить свои наработки.
  
  Спасибо за мотивацию. Сейчас у меня есть результат, который я могу улучшать и дорабатывать при этом используя 
  его в каких-то проектах.

-------------------


### Порядок для запуска проектов (частей проекта) ###


#### Рекомендованный ####

1) Bridge

2) Database

3) Yii2

4) Processing

5) Dashboard


-------------------


#### Зависимости частей проекта ####

`Yii2` работает с запущенными `Bridge` и `Database`.

`Processing` работает с запущенным `Bridge`.

`Dashboard` работает с запущенным `Bridge`


-------------------


## Общее окружение всех частей проекта ##


-------------------


    Docker: *
    PHP: 7.3
    PHP: 5.6 (только в Gearman Dashboard)
    Gearmand: 1.0.6
    Yii: 2.0.40
    MySQL: 5.7.30
    Nginx: 1.19.0 (только в Yii2 Client App)
    Apache: 2.4.25 (только в Gearman Dashboard)


-------------------


## Установка ##


-------------------


1) Установка Bridge:

```shell
docker network create le-net
docker-compose up -d
```

* В этом проекте есть два образца для файла `docker-compose.yml`:
  
   1) [/docs/config-example/with-db.yml](/docs/config-example/with-db.yml)
        
      * Этот вариант (он же текущий) настроек Docker compose файла для Gearman целостности сообщений.
        Все задачи которые попадают в очередь, так же сохранияются в базу данных MySQL. Если Gearman
        завершится, при его запуске все задачи будут восстановлены из базы данных.
        
   2) [/docs/config-example/without-db.yml](/docs/config-example/without-db.yml)

      * Если нет необходимости в целостности данных, и необходимо больше скорости.

2) Установка остальных проектов (частей проекта):

Клонировать другие проекты. Проекты-приложения (`Yii2`, `Processing`) должны в себе содержать
ссылку на директорию из этого проекта. Эта общая директория расположена `/.docker/mount/cdn`.
Согласно `docker-compose` файлам, ссылки на директорию должны быть расположены по такому же пути 
`%another_project%/.docker/mount/cdn`.

3) Некоторые проекты (`Yii2`, `Processing`) имеют свои тонкости для запуска/установки в каждом из
   `/README.md` в своих проектах.


-------------------


## Для демонстрации ##

Согласно docker compose файлам Yii2 будет доступно по ссылке `http://127.0.0.1:39104`.
Одна из миграций с сидом тестов с подготовленным/генерируемым набором данных.

Доступы для тестового пользователя:

```
    username: system
    password: admin
```

Настройка количества обработчиков файлов в проекте `Processing` происходит из проекта `Yii2 Client Application`.
В файле `/common/config/params.php` есть параметр `queue.service.store.productImport.parallelHandlers`.
При перезапуске приложения оно будет автоматически отправлять сообщение на `Processing` о том, какое количество
обработчиков ему необходимо.

На стороне `Yii2 Client Application` не реализовывалось, хоть и можно было сделать аналогичную
сервису схему запуска обработчиков. Там реализована простой запуск приложений в фоновом процессе.
Можно было бы использовать supervisord для настройки и запуска, но как более быстрый способ команды
просто добавлены в секцию CMD docker образа. Запускаемые команды описаны в файле `.docker/images/php/cmd.sh`.
Если необходимо будет отредактировать количество запускаемых обработчиков в этом файле можно продублировать необходимую
строку.
