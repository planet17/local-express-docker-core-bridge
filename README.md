# Local Express: Bridge #

https://bitbucket.org/planet17/local-express-docker-core-bridge

Just container what contains queue image and separated mysql for queue.


-------------------


Whole project development with [Docker](https://www.docker.com/).

`Bridge` is core docker-container for project.

Russian language here is [/docs/RU-README.md](/docs/RU-README.md)

That project contains queue (Gearman) image, database for queue image, docs
with description for instruction. Also, it contains storage directory for files
(like an easy way to implement CDN).
Also, that project contains common `.env.example` file.

Here is [/docs/RU-SPECIFICATION.md](/docs/RU-SPECIFICATION.md) could be found a
task for project.


## Environment ##


-------------------


    Docker: *
    Mysql: 5.7.30
    Gearmand 1.0.6


-------------------


## Parts of project ##


Test specification completed with use separated docker containers such as:

*Required:*

1) [Bridge (current)](https://bitbucket.org/planet17/local-express-docker-core-bridge) - contains queue image, docs.

2) [Database](https://bitbucket.org/planet17/local-express-docker-storage-mysql) - contains core database with all data.

3) [Yii2 - Client Application](https://bitbucket.org/planet17/local-express-docker-client-app) - clients app.

4) [Processing](https://bitbucket.org/planet17/local-express-docker-processing-app) - backend service app.


*Optional:*

5) [Dashboard for Gearman](https://bitbucket.org/planet17/local-express-docker-gearman-dashboard) -
   use for watching state of workers, queue.

### Non project (not for separate installation) but also included ###

#### Domain library ####

[CQO](https://bitbucket.org/planet17/local-express-common-queue-objects) - library
uses at both application (`Processing` and `Yii2`). It contains some common elements for message queue.

#### Non domain libraries but written by me for works with message queue: ####

* [https://bitbucket.org/planet17/message-queue-library](https://bitbucket.org/planet17/message-queue-library)
* [https://bitbucket.org/planet17/message-queue-library-route-nav](https://bitbucket.org/planet17/message-queue-library-route-nav)
* [https://bitbucket.org/planet17/message-queue-library-process-manager](https://bitbucket.org/planet17/message-queue-library-process-manager)
* [https://bitbucket.org/planet17/cli-processes](https://bitbucket.org/planet17/cli-processes)
* [https://bitbucket.org/planet17/application-process-manager-routed-queue](https://bitbucket.org/planet17/application-process-manager-routed-queue)
* [https://bitbucket.org/planet17/message-queue-library-pipeline-handlers](https://bitbucket.org/planet17/message-queue-library-pipeline-handlers)


-------------------

##### Why I did these libraries? #####

* Long I time ago... I planned to do my pretty wrappers for work with different queue drivers.
  I tried it finishing few times. I have only sketches, and some troubles as results.
  Your test specification motivated me to remove all sketches, start it from blank, redesign it and complete.
  
  Thanks for motivation. Now I could improve result, and use it for other projects.

-------------------


### Order for running projects ###


#### Recommended way ####

1) Bridge

2) Database

3) Yii2

4) Processing

5) Dashboard


-------------------


#### Projects dependencies ####

`Yii2` depends on `Bridge` and `Database`.

`Processing` depends on `Bridge`.

`Dashboard` depends on `Bridge`


-------------------


## Common environment ##


-------------------


    Docker: *
    PHP: 7.3
    PHP: 5.6 (only for Gearman Dashboard)
    Gearmand: 1.0.6
    Yii: 2.0.40
    MySQL: 5.7.30
    Nginx: 1.19.0 (only for Yii2 Client App)
    Apache: 2.4.25 (only for Gearman Dashboard)


-------------------


## Installation ##


-------------------


1) Bridge installation:

```shell
docker network create le-net
docker-compose up -d
```

* There is two example for `docker-compose.yml`:
  
   1) [/docs/config-example/with-db.yml](/docs/config-example/with-db.yml)
        
      * That (current) settings at Docker compose file for Gearman with persistent safe-mode.
        All queued tasks storing to MySQL. If Gearman is shut down, at the startup all
        tasks will be restored from the database.
        
   2) [/docs/config-example/without-db.yml](/docs/config-example/without-db.yml)

      * When project doesn't need persistence and need more speed.

2) Install another projects:

Cloning all another projects. Project with applications (`Yii2`, `Processing`) must have symlink
to directory from current project. Project must be shared is located at `/.docker/mount/cdn`.
By `docker-compose` links must be located at same place `%another_project%/.docker/mount/cdn`.

3) Some projects  (`Yii2`, `Processing`) have some details for installation check it and follow them in `/README.md`


-------------------


## Demo ##

By docker compose file yii2 client app available with link `http://127.0.0.1:39104`.
At yii2 migrations one of them it is seed with tests set of data.

Test credentials for user:

```
    username: system
    password: admin
```

Setup of amount handlers of file at `Processing` from `Yii2 Client 
Application`. 
File `/common/config/params.php` contains param `queue.service.store.productImport.parallelHandlers`.
While restart/up application/container that will be automatically send message with amount to `Processing` for 
configuring number of working handlers.

I didn't make same scheme for manage amount of handlers for project `Yii2 Client Application` same schema didn't. 
Without that scheme it could be supervisord. I use the easiest stupid straight way for it. Add into CMD docker section 
commands  running like background process at file `.docker/images/php/cmd.sh`.
For changing amount of daemons you add some or remove some line from that file..
